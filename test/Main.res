let tests = [
  Assert.equality({
    expected: "ipsum",
    message: "Strings should be the same, [will fail]",
    operator: "lorem ipsum operation",
    comparator: (a, e) => a === e,
    fn: () => "lorem"
  }),
  Assert.condition({
    message: "Function should return true.",
    operator: "trivial condition",
    fn: () => true
  }),
  Assert.condition({
    message: "Function may throw (it will)",
    operator: "trivial throw",
    fn: () => Js.Exn.raiseError("it threw!")
  })
];
                  
                  
let mainSuite: Suite.t = { suiteName: "main", asserts: tests };

let batchConclusion = Test.runBatch(Configuration.default, [mainSuite]);


/* for demo purposes, I want to show one of each.*/
if (batchConclusion.totalFailures === 1
    && batchConclusion.totalSuccesses === 1
    && batchConclusion.totalExceptions === 1
   ) {
  Print.good("Demo tests ran as expected.", Configuration.default);
}
else { Js.Exn.raiseError("Unexpected result count."); }
