let line = (text: string, color: string => string): unit =>
    switch (Process.process) {
        | None => Js.Console.log(text);
        | Some(_) => Js.Console.log(color(text));
    };

let bad = (text: string, config: Configuration.t): unit =>
    line(text, config.colorFail);

let good = (text: string, config: Configuration.t): unit =>
    line(text, config.colorPass);

let either = (text: string, config: Configuration.t, condition: bool): unit =>
    condition ? good(text, config) : bad(text, config);

let neutral = (text: string, config: Configuration.t): unit =>
    line(text, config.colorNeutral);


