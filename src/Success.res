type t = {
  index: int, // 1-indexed number of test in suite
  suiteLength: int,
  assertion: Assert.t
}

let toPrint = (s: t): string =>
    `[ ${s.index -> Belt.Int.toString} / `
    -> Js.String2.concat(`${s.suiteLength -> Belt.Int.toString} ] `)
    -> Js.String2.concat(`${s.assertion.operator}\n`)
    -> Js.String2.concat(`\tMessage: ${s.assertion.message}\n`)
    -> Js.String2.concat(`\tPASS`);



let toXML = (s: t): string =>
    `<testcase id="${s.assertion.message}" name="${s.assertion.operator}"/>`
