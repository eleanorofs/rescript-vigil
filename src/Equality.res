type t<'sut> = {
  expected: 'sut,
  message: string,
  operator: string,
  comparator: ('sut, 'sut) => bool,
  fn: unit => 'sut
};
