type t = {
  conclusions: array<Conclusion.t>,
  suiteName: string,
  totalSuccesses: int,
  totalFailures: int,
  totalExceptions: int
};

let analyze = (conclusions: array<Conclusion.t>, suiteName: string): t => {
  conclusions: conclusions,
  suiteName: suiteName,
  totalSuccesses: conclusions
    -> Js.Array2.filter(Conclusion.isSuccess)
    -> Js.Array2.length,
  totalFailures: conclusions
    -> Js.Array2.filter(Conclusion.isFailure)
    -> Js.Array2.length,
  totalExceptions: conclusions
    -> Js.Array2.filter(Conclusion.isException)
    -> Js.Array2.length
};

let failuresXML = (conclusion: t): string =>
    Belt.Int.toString(conclusion.totalFailures + conclusion.totalSuccesses);

let innerXML = (t: t): string =>
    t.conclusions -> Js.Array2.reduce((s, c) =>
      Js.String2.concat(s,Conclusion.toXML(c)), "");

let isSuccessful = (conclusion: t): bool =>
    conclusion.totalFailures + conclusion.totalExceptions === 0;

let print = (conclusion: t, config: Configuration.t): unit => 
    `${conclusion.suiteName} suite finished running. \n`
    -> Js.String2.concat(conclusion.totalSuccesses -> Belt.Int.toString)
    -> Js.String2.concat(` successes. \n`)
    -> Js.String2.concat(conclusion.totalFailures -> Belt.Int.toString)
    -> Js.String2.concat(` failures. \n`)
    -> Js.String2.concat(conclusion.totalExceptions -> Belt.Int.toString)
    -> Js.String2.concat(` exceptions. \n`)
    -> Print.either(config, isSuccessful(conclusion))
;

let testsXML = (t: t): string =>
    Belt.Int.toString(t.totalSuccesses + t.totalFailures + t.totalExceptions);

let toXML = (t: t): string =>
    `<testsuite id="${t.suiteName}" message="${t.suiteName}" `
    -> Js.String2.concat(`tests="${testsXML(t)}" `)
    -> Js.String2.concat(`failures="${failuresXML(t)}"`)
    -> Js.String2.concat(`>`)
    -> Js.String2.concat(t -> innerXML)
    -> Js.String2.concat("</testsuite> \n");
