type t = {
  message: string,
  operator: string,
  fn: () => bool
};
