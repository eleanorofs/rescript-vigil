type t = Success(Success.t) | Failure(Failure.t) | Exception(Exception.t);

let isSuccess = (conclusion: t): bool =>
    switch(conclusion) {
        | Success(_) => true;
        | Failure(_) => false;
        | Exception(_) => false;
    };

let isFailure = (conclusion: t): bool =>
    switch(conclusion) {
        | Success(_) => false;
        | Failure(_) => true;
        | Exception(_) => false;
    };

let isException = (conclusion: t) =>
    switch(conclusion) {
        | Success(_) => false;
        | Failure(_) => false;
        | Exception(_) => true;
    };



let print = (t: t, config: Configuration.t): unit =>
    switch (t) {
        | Success(s) => s -> Success.toPrint -> Print.good(config);
        | Failure(f) => f -> Failure.toPrint -> Print.bad(config);
        | Exception(e) => e -> Exception.toPrint -> Print.bad(config);
    };

let toXML = (t: t): string => 
    switch (t) {
        | Success(s) => s -> Success.toXML;
        | Failure(f) => f -> Failure.toXML;
        | Exception(e) => e -> Exception.toXML;
    };
