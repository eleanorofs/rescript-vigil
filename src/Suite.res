type t = {
  suiteName: string,
  asserts: array<Assert.t>
};
