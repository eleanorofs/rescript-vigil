type t = {
  /* color printed to console to show errors and failures */
  colorFail: string => string,
  /* color printed to console to show passing tests */
  colorNeutral: string => string,
  /* color printed to console to show neutral output */
  colorPass: string => string,
  /* Console, Silent, XML */
  output: OutputMode.t
};

let default = {
  colorFail: Color.red,
  colorNeutral: Color.default,
  colorPass: Color.green,
  output: OutputMode.Console
};
