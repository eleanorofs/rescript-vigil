type t = {
  index: int, // 1-indexed number of test in suite
  suiteLength: int,
  assertion: Assert.t,
  ex: Js.Exn.t
};

let stringifyExn = (ex: Js.Exn.t): string =>
    ex -> Js.Json.stringifyAny -> Belt.Option.getWithDefault("undefined");

let toPrint = (f: t): string =>
    `[ ${f.index -> Belt.Int.toString} / `
    -> Js.String2.concat(`${f.suiteLength -> Belt.Int.toString} ] `)
    -> Js.String2.concat(`${f.assertion.operator}\n`)
    -> Js.String2.concat(`\tMessage: ${f.assertion.message}\n`)
    -> Js.String2.concat(`\tExpected: `)
    -> Js.String2.concat(f.assertion.expected -> Sut.stringify)
    -> Js.String2.concat(`\n`)
    -> Js.String2.concat(`\tException: ${f.ex -> stringifyExn}\n`)
    -> Js.String2.concat(`\tFunction: ${Sut.toSutFnString(f.assertion.fn)}\n`)
    -> Js.String2.concat(`\tEXCEPTION`);

let toXML = (f: t): string =>
    `<testcase id="${f.assertion.message}" name="${f.assertion.operator}">\n`
    -> Js.String2.concat(`<failure message="${f.assertion.message}"`)
    -> Js.String2.concat(`type="exception">\n`)
    -> Js.String2.concat(`${f.assertion.operator}\n`)
    -> Js.String2.concat(`\tMessage: ${f.assertion.message}\n`)
    -> Js.String2.concat(`\tExpected: `)
    -> Js.String2.concat(f.assertion.expected -> Sut.stringify)
    -> Js.String2.concat(`\n`)
    -> Js.String2.concat(`\tException: ${f.ex -> stringifyExn}\n`)
    -> Js.String2.concat(`\tFunction: ${Sut.toSutFnString(f.assertion.fn)}\n`)
    -> Js.String2.concat(`</failure>`)
    -> Js.String2.concat(`</testcase>`);

