let red = (text: string): string => `\x1b[31m${text}\x1b[39m`;
let green = (text: string): string => `\x1b[32m${text}\x1b[39m`;
let default = (text: string): string => `\x1b[0m${text}\x1b[0m`; 
