type t;

type sutComparator = (t, t) => bool;

type sutFn = () => t;

let stringify = (sut: t): string =>
    sut -> Js.Json.stringifyAny
    -> Belt.Option.getWithDefault("undefined");

external toSut: 'sut => t = "%identity";

external toSutComparator: (('sut, 'sut) => bool) => sutComparator
  = "%identity";

external toSutFn: (() => 'sut) => sutFn = "%identity";

@send
external toSutFnString: sutFn => string = "toString";

