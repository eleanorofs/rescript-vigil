type t = {
  comparator: (Sut.t, Sut.t) => bool,
  message: string,
  operator: string,
  expected: Sut.t,
  fn: () => Sut.t
};

let condition = (cond: Condition.t): t => {
  comparator: ((a, _) => a === true) -> Sut.toSutComparator,
  message: cond.message,
  operator: cond.operator,
  expected: true -> Sut.toSut,
  fn: cond.fn -> Sut.toSutFn
};

let equality = (eq: Equality.t<'sut>): t => {
  comparator: eq.comparator -> Sut.toSutComparator,
  message: eq.message,
  operator: eq.operator,
  expected: eq.expected -> Sut.toSut,
  fn: eq.fn -> Sut.toSutFn
};
