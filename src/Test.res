@val external clear: () => unit = "console.clear";
  
let runBatch = (config: Configuration.t, suites: array<Suite.t>):
    BatchConclusion.t => {
      switch (config.output) {
          | Console => Noisy.runBatch(suites, config);
          | Silent => Silent.runBatch(suites, config);
          | XML => {
            let result = Silent.runBatch(suites, config);
            result -> BatchConclusion.toXML -> Js.Console.log
            result;
          }
      };
    };
