let run =(assertion: Assert.t,
          index: int,
          suiteLength: int,
          config: Configuration. t): Conclusion.t
  => {
    let result = assertion -> Silent.conclude(index + 1, suiteLength);
    result -> Conclusion.print(config);
    result;
};


let runSuite = (asserts: array<Assert.t>,
                suiteName: string,
                config: Configuration.t): SuiteConclusion.t
  => {
    `${suiteName}: `
      -> Js.String2.concat(asserts -> Js.Array2.length-> Belt.Int.toString)
      -> Js.String2.concat(` test cases...`)
      -> Print.neutral(config);
    let conclusions = asserts ->
        Js.Array2.mapi((assertion, i) =>
          assertion -> run(i, asserts -> Js.Array2.length, config));
    let result = conclusions -> SuiteConclusion.analyze(suiteName);
    result -> SuiteConclusion.print(config);
    result;
};
  
let runBatch = (suites: array<Suite.t>, config: Configuration.t):
    BatchConclusion.t => {
      "Starting batch of test suites..." -> Print.neutral(config);
      let result = suites
          -> Js.Array2.map(suite =>
            runSuite(suite.asserts, suite.suiteName, config))
          -> BatchConclusion.analyze;
      
      result -> BatchConclusion.print(config);
      result;
    };
