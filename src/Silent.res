let conclude = (assertion: Assert.t,
                index: int,
                suiteLength: int): Conclusion.t
  => {
    try {
      let actual = assertion.fn();
      actual -> assertion.comparator(assertion.expected)
        ? Success({index: index,
                   suiteLength: suiteLength,
                   assertion: assertion
                  })
        : Failure({index: index,
                   suiteLength: suiteLength,
                   assertion: assertion,
                   actual: actual
                  });
    }
    catch {
        | Js.Exn.Error(obj) => Exception({index: index,
                                          suiteLength: suiteLength,
                                          assertion: assertion,
                                          ex: obj
                                         });
    };
  };

let run =(assertion: Assert.t,
          index: int,
          suiteLength: int,
          config: Configuration. t): Conclusion.t
  => assertion -> conclude(index + 1, suiteLength);



let runSuite = (asserts: array<Assert.t>,
                suiteName: string,
                config: Configuration.t): SuiteConclusion.t
  =>
asserts ->
  Js.Array2.mapi((assertion, i) =>
    assertion -> run(i, asserts -> Js.Array2.length, config))
  -> SuiteConclusion.analyze(suiteName);

let runBatch = (suites: array<Suite.t>, config: Configuration.t):
    BatchConclusion.t => {
      suites
        -> Js.Array2.map(suite =>
          runSuite(suite.asserts, suite.suiteName, config))
        -> BatchConclusion.analyze;
};
