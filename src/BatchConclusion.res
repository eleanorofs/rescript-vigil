type t = {
  conclusions: array<SuiteConclusion.t>,
  totalSuccesses: int,
  totalFailures: int,
  totalExceptions: int
};

let analyze = (suites: array<SuiteConclusion.t>): t => {
  conclusions: suites,
  totalSuccesses: suites
    -> Js.Array2.reduce((a, s) => a + s.totalSuccesses, 0),
  totalFailures: suites
    -> Js.Array2.reduce((a, s) => a + s.totalFailures, 0),
  totalExceptions: suites
    -> Js.Array2.reduce((a, s) => a + s.totalExceptions, 0)
};

let failures = (t: t): string =>
    Belt.Int.toString(t.totalFailures + t.totalExceptions);

let innerXML = (t: t): string =>
    t.conclusions -> Js.Array2.reduce((s, c) =>
      Js.String2.concat(s, SuiteConclusion.toXML(c)), "");

let isSuccessful = (conclusion: t): bool =>
    conclusion.totalFailures + conclusion.totalExceptions === 0;


let print = (conclusion: t, config: Configuration.t): unit => 
    `Suite finished running. \n`
    -> Js.String2.concat(conclusion.totalSuccesses -> Belt.Int.toString)
    -> Js.String2.concat(` successes. \n`)
    -> Js.String2.concat(conclusion.totalFailures -> Belt.Int.toString)
    -> Js.String2.concat(` failures. \n`)
    -> Js.String2.concat(conclusion.totalExceptions -> Belt.Int.toString)
    -> Js.String2.concat(` exceptions. \n`)
    -> Print.either(config, isSuccessful(conclusion))
;

let testsXML = (t: t): string =>
    Belt.Int.toString(t.totalSuccesses + t.totalFailures + t.totalExceptions);

let toXML = (t: t): string =>
    `<xml version="1.0" encoding="utf-8" ?>\n`
    -> Js.String2.concat(`<testsuites id="vigil-test-run"`)
    -> Js.String2.concat(`tests="${testsXML(t)}" failures="${failures(t)}">\n`)
    -> Js.String2.concat(t -> innerXML)
    -> Js.String2.concat(`</testsuites>`);

