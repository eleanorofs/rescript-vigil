# rescript-vigil

**This package is in beta. Expect breaking changes!**

a minimal testing library for ReScript, compatible with ES6 modules.

## Guiding Philosophy

### Simplicity

While I can't commit to *never* adding any runtime dependencies, I can commit
to taking on as few as possible. 

I also don't intend to load this library up with features. This is a codebase
that anyone should be able to easily read, understand, and fork. 

### ES6-Compatibility

This library was created in part because `rescript-test` stopped supporting 
ES6 modules, and ES6 module compatibility will always be a priority for this
project. That said, if there's anything I can do to better support commonjs
modules, please open an issue or a PR, and we can talk about it. 

### Extensibility and Interoperability

Please note that this is a *testing library*, not a testing *framework*. This
is intentional. It is meant to be used with the mocking library or browser
emulator of your choice and run not in its own special runtime but as a script
in your console or browser. 

This may mean some additional work is required of the user. For example, for 
a test script to fail, you should throw an exception if the number of passing
tests isn't equal to the number of total tests. On the other hand, this opens
up opportunities to be flexible, such as the ability to manually write 
additional messages to the
console with `Print.good` or `Print.bad`, which can make your results more 
readable. 

## Installation

```bash
npm config set @eleanorofs:registry https://gitlab.com/api/v4/packages/npm/
npm i --save-dev @eleanorofs/rescript-vigil
```

## Concepts

The test suite for this package is itself a simple use case. To break it down:

* There are two kinds of `Assert`s: `Condition`s and `Equality`s.
* `Assert`s can be run individually
  - through `Silent.conclude`, `Silent.run`, or `Noisy.run`.
* `Assert`s can be run as part of a `Suite`, which is a named list of 
`Assert`s
* `Suite`s can be run individually
  - through `Silent.runSuite` or
  - through `Noisy.runSuite`
* `Suite`s can also be run as a batch, which is just an `array<Suite.t>` 
through `Test.runBatch`. 

(I dabbled with some JUnit XML, but that part of it hasn't been tested in the
real world, so please avoid this feature unless you're willing to contribute
helpful issues.)

## Usage 

The following is the code that runs the test suite for this package. This gives
an example of one passing test, one failing test, and one test which threw an
exception.

```js
let tests = [
  Assert.equality({
    expected: "ipsum",
    message: "Strings should be the same, [will fail]",
    operator: "lorem ipsum operation",
    comparator: (a, e) => a === e,
    fn: () => "lorem"
  }),
  Assert.condition({
    message: "Function should return true.",
    operator: "trivial condition",
    fn: () => true
  }),
  Assert.condition({
    message: "Function may throw (it will)",
    operator: "trivial throw",
    fn: () => Js.Exn.raiseError("it threw!")
  })
];
             
let mainSuite: Suite.t = { suiteName: "main", asserts: tests };

let batchConclusion = Test.runBatch(Configuration.default, [mainSuite]);


/* for demo purposes, I want to show one of each.*/
if (batchConclusion.totalFailures === 1
    && batchConclusion.totalSuccesses === 1
    && batchConclusion.totalExceptions === 1
   ) {
  Print.good("Demo tests ran as expected.", Configuration.default);
}
else { Js.Exn.raiseError("Unexpected result count."); }
```

## Results

You can see a sample output from this project's 
[CI/CD pipline](https://gitlab.com/eleanorofs/rescript-vigil/-/jobs/1978281412).
(The failures are intentional for demonstration purposes.)

## License
This software is available under two licenses. 

* an adaptation of the Do No Harm license which I've called the No Violence
license. 
* the MIT license.

Both are available in this directory. 

